package android.jjang.redditreader;

public class RedditPost {

    public String title;
    public String url;

    public RedditPost(String title, String url){
        this.title = title;
        this.url = url;
    }
}
