package android.jjang.redditreader;

import android.support.v4.app.Fragment;

public class RedditActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new RedditListFragment();
    }

}
